# Базы данных.

## Семинар 9. SCD. Типы SCD. Работа с версионными таблицами. Разграничение доступов PostgreSQL. DCL

### 1. Теоретическая справка

#### 1.1 SCD. Типы SCD

**SCD (Slowly Changing Dimension)** —  редко изменяющиеся измерения, то есть измерения, не ключевые атрибуты которых имеют тенденцию со временем изменяться.

Выделяют пять основных типов:

* **SCD 0**: после попадания в таблицу данные никогда  не изменяются — не поддерживает версионность. Практически никогда не используется.
* **SCD 1**: данные перезаписываются (всегда актуальные) — используется, если история не нужна.
    * _Достоинства:_
        * Не добавляет избыточность;
        * Очень простая структура.
    * _Недостатки:_
        * Не хранит историю.
* **SCD 2**: храним историю с помощью двух полей (дата начала действия, дата окончания действия: `valid_from`, `valid_to`) — _используется наиболее часто_.
    * _Достоинства:_
        * Хранит полную и неограниченную историю версий;
        * Удобный и простой доступ к данным необходимого периода.
    * _Недостатки:_
        * Провоцирует на избыточность или заведение доп. таблиц для хранения изменяемых атрибутов.

![](img/img_scd2.png)

Вместо `NULL` используется некоторая константа (для `valid_to — '9999-12-31'`), что позволяет писать простое условие:
```sql
WHERE day_dt BETWEEN valid_from_dttm AND valid_to_dttm
```
вместо
```sql
WHERE day_dt >= valid_from_dttm
    AND (day_dt <= valid_to_dttm
        OR valid_to_dttm IS NULL)
```

* **SCD 3**: храним старое значение для выделенных атрибутов в отдельных полях. При получении новых данных старые данные перезаписываются текущими значениями.
    * _Достоинства:_
        * Небольшой объем данных;
        * Простой и быстрый доступ к истории.
    * _Недостатки:_
        * Ограниченная история.

![](img/img_scd3.png)

* **SCD 4**: храним в основной таблице только актуальные данные и историю изменений в отдельной таблице. Основная таблица всегда перезаписывается текущими данными с перенесением старых данных в другую таблицу. Обычно этот тип используют для аудита изменений или создания архивных таблиц.
    * _Достоинства:_
        * Быстрая работа с текущими версиями
    * _Недостатки:_
        * Разделение единой сущности на разные таблицы

![](img/img_scd4.png)

#### CASE-выражения

```sql
CASE [expression]     -- expression – проверяемое значение
    WHEN condition_1 THEN result_1
    WHEN condition_2 THEN result_2
    ...
    WHEN condition_N THEN result_N
    ELSE result
END
```

Пример без использования `expression`:
```sql
  CASE
    WHEN Salary >= 3000 THEN 'ЗП >= 3000'
    WHEN Salary >= 2000 THEN '2000 <= ЗП < 3000'
    ELSE 'ЗП < 2000'
  END SalaryTypeWithELSE,
```

Пример с использованием `expression`:
```sql
  CASE DepartmentID     -- проверяемое значение
    WHEN 2 THEN '10%'   -- 10% от ЗП выдать сотрудникам отдела 2
    WHEN 3 THEN '15%'   -- 15% от ЗП выдать сотрудникам отдела 3
    ELSE '5%'           -- всем остальным по 5%
  END NewYearBonusPercent
```

### 2.0 Типовые задачи

* Поддержка версионности таблиц
* Соединение версионных таблицы
* Поиск разрывов в версионности
* Поиск пересечений в версионности
* Сравнение таблиц

### 2.1 Соединение версионных таблиц

![](img/img_scd_merge.png)

**Как соединяем?**
1. Отделы в соединяемых записях должны совпадать:
```sql
    EMP_DEPT.DEPT_NO = DEPT.DEPT_NO
```
2. Конкретная строка из таблицы `EMP_DEPT` (исходная строка) должна соединяться только с теми строками таблицы DEPT (соединяемая строка), у которых период действия покрывает часть периода действия исходной строки:
```sql
    DEPT.START_DATE <= EMP_DEPT.END_DATE
    AND DEPT.END_DATE >= EMP_DEPT.START_DATE
```
3. Датой начала действия результирующего интервала должна быть максимальная дата из дат начала действия исходного и присоединяемого интервала:
```sql
    START_DATE = max(EMP_DEPT.START_DATE, DEPT.START_DATE)
```
4. Датой окончания действия результирующего интервала должна быть минимальная дата из дат окончания действия исходного и присоединяемого интервала:
```sql
    END_DATE = min(EMP_DEPT.END_DATE, DEPT.END_DATE)
```

```sql
SELECT emp_dept.emd_name
    , dept.dept_name
    , CASE
        WHEN emp_dept.start_date > dept.start_date
        THEN emp_dept.start_date
        ELSE dept.start_date
    END AS start_date
    , CASE
        WHEN emp_dept.end_date < dept.end_date
        THEN emp_dept.end_date
        ELSE dept.end_date
    END AS end_date
FROM emp_date
INNER JOIN dept
    ON emp_date.dept_no = dept.dept_no
    AND dept.start_date <= emp_dept.end_date
    AND dept.end_date >= emp_dept.start_date;
```

```sql
SELECT emp_dept.emd_name
    , dept.dept_name
    , greatest(emp_dept.start_date, dept.start_date) AS start_date
    , least(emp_dept.end_date, dept.end_date) AS end_date
FROM emp_date
INNER JOIN dept
    ON emp_date.dept_no = dept.dept_no
    AND dept.start_date <= emp_dept.end_date
    AND dept.end_date >= emp_dept.start_date;
```

![](img/img_merge_scd_meme.png)

### 2.2 Поиск разрывов в версионности

![](img/img_scd_gap.png)

В таблице присутствуют 2 разрыва:
1. Для сотрудника 'JONES’ нет информации о его работе в период с ‘1997-11-11’ по ‘1999-03-04’;
2. Для сотрудника ‘BLAKE’ нет информации о его работе в период с ‘2002-05-16’ по ‘2002-05-17’.

Возможные причины:
* Логика исходных данных – сотрудник ‘JONES’ не работал в компании с ‘1997-11-11’ по ‘1999-03-04’
* Ошибка ввода данных – у сотрудника ‘BLAKE’ неправильно введена дата начала его работы в отделе 3 или дата окончания его работы в отделе 2

**Как ищем разрывы?**
1. Используем аналитические функции;
2. Разбиваем данные на группы по ключу таблицы `EMPLOYEE_NM`
3. Отсортировываем данные в группах по дате начала действия интервала `VALID_FROM_DTTM`
4. Проверяем, превышает ли разница между датой начала действия текущего интервала и датой окончания действия предыдущего интервал в 1 секунду.

```sql
select employee_nm
    , prev_valid_to_dttm
    , valid_from_dttm
from (
    select employee_nm
        , lag(valid_to_dttm)
            over (partition by employee_nm
                order by valid_from_dttm) as prev_valid_to_dttm
        , valid_from_dttm
    from emp_dept
) t
where valid_from_dttm - prev_valid_to_dttm > interval '1 sec';
```

![](img/img_scd_gap1.png)

Так можем получить промежутки, в которых не хватает данных:

```sql
select employee_nm
    , prev_valid_to_dttm + interval ‘1 sec’ as miss_valid_from_dttm
    , valid_from_dttm - interval ‘1 sec’ as miss_valid_to_dttm
from (
    select employee_nm
    , lag(valid_to_dttm)
        over (partition by employee_nm
            order by valid_from_dttm) as prev_valid_to_dttm
    , valid_from_dttm
from emp_dept
) t
where valid_from_dttm – prev_valid_to_dttm > interval ‘1 sec’;
```

![](img/img_scd_gap2.png)

### 2.3 Поиск пересечений в версионности

![](img/img_scd_intersect.png)

В таблице присутствуют 2 пересечения:
1. Сотрудник ‘JONES’ работал в двух отделах в период с ‘1999-03-05’ по ‘1999-07-20’
2. Сотрудник ‘BLAKE’ работал в двух отделах в период с ‘2002-05-18’ по ‘2002-05-20’

**Как ищем пересечения?**
1. Используем аналитические функции;
2. Разбиваем данные на группы по ключу таблицы `EMPLOYEE_NM`
3. Отсортировываем данные в группах по дате начала действия интервала `VALID_FROM_DTTM`
4. Проверяем, не начинается ли период действия текущей записи раньше, чем заканчивается предыдущий интервал.

```sql
select employee_nm
    , prev_valid_from_dttm
    , prev_valid_to_dttm
    , valid_from_dttm
    , valid_to_dttm
from (
    select employee_nm
        , lag(valid_from_dttm)
            over (partition by employee_nm
                order by valid_from_dttm) as prev_valid_from_dttm,
        , lag(valid_to_dttm)
            over (partition by employee_nm
                order by valid_from_dttm) as prev_valid_to_dttm,
        , valid_from_dttm
        , valid_to_dttm
    from emp_dept
    ) t
where valid_from_dttm <= prev_valid_to_dttm;
```

*Примечание:*
* Можно решить эту задачу без аналитический функций простым соединением версионных таблиц
* Условия использования такого решения:
    * В качестве соединяемых таблиц будет выступать одна и та же таблица
    * Наличие возможности отличить любые две строки таблицы
    * Нет строк с одинаковым ключом и периодом действия

```sql
select e1.employee_name
    , e1. as prev_valid_from_dttm
    , e1.valid_to_dttm as prev_valid_to_dttm
    , e2.valid_from_dttm
    , e2.valid_to_dttm
from emp_dept e1
inner join emp_dept e2
    on e1.employee_nm = e2.employee_nm
    and e2.valid_from_dttm between e1.valid_from_dttm and e1.valid_to_dttm
    and e1.valid_to_dttm <> e2.valid_to_dttm;
```

### 3. Разграничение доступов PostgreSQL. Data Control Language (DCL)

#### 3.1 Разграничение доступов PostgreSQL

**Роль** – множество пользователей БД
* Может владеть объектами в базе
* Может иметь определенный доступ к некоторым объектам базы, не являясь их владельцем
* Может выдавать доступы на некоторые объекты в базе другим ролям
* Может предоставлять членство в роли другой роли

**Создание пользователя**

```sql
CREATE USER == CREATE ROLE

CREATE USER name [[WITH] option [...]]
where option can be:
SUPERUSER | NOSUPERUSER
| CREATEDB | NOCREATEDB
| CREATEROLE | NOCREATEROLE
| INHERIT | NOINHERIT
| LOGIN | NOLOGIN
| REPLICATION | NOREPLICATION
| BYPASSRLS | NOBYPASSRLS
| CONNECTION LIMIT connlimit
| [ENCRYPTED] PASSWORD 'password' | PASSWORD NULL
| VALID UNTIL 'timestamp'
| IN ROLE role_name [, ...]
| IN GROUP role_name [, ...]
| ROLE role_name [, ...]
| ADMIN role_name [, ...]
| USER role_name [, ...]
| SYSID uid
```

```sql
CREATE ROLE jonathan LOGIN;

CREATE USER davide
WITH PASSWORD 'jw8s0F4';

CREATE ROLE Miriam
WITH LOGIN PASSWORD 'jw8s0F4'
VALID UNTIL '2005-01-01';
```

#### 3.2 Data Control Language (DCL)

* Позволяет настраивать доступы к объектам
* Поддерживает 2 типа действий:
    * `GRANT` – выдача доступа к объекту
    * `REVOKE` – отмена доступа к объекту

**Права, которые можно выдать на объект:**
* SELECT
* INSERT
* UPDATE
* DELETE
* TRUNCATE
* REFERENCES
* TRIGGER
* CREATE
* CONNECT
* TEMPORARY
* EXECUTE
* USAGE

[**GRANT ON TABLE**](https://postgrespro.ru/docs/postgresql/9.6/sql-grant)
```sql
GRANT {{SELECT | INSERT | UPDATE | DELETE | TRUNCATE |
    REFERENCES | TRIGGER}
[, ...] | ALL [PRIVILEGES]}
ON {[ TABLE] table_name [, ...]
    | ALL TABLES IN SCHEMA schema_name [, ...]}
TO role_specification [, ...] [WITH GRANT OPTION]
```

[**REVOKE ON TABLE**](https://postgrespro.ru/docs/postgresql/9.6/sql-revoke)

```sql
REVOKE [GRANT OPTION FOR]
    {{SELECT | INSERT | UPDATE | DELETE | TRUNCATE |
        REFERENCES | TRIGGER}
[, ...] | ALL [PRIVILEGES]}
ON {[TABLE] table_name [, ...]
    | ALL TABLES IN SCHEMA schema_name [, ...]}
FROM {[GROUP] role_name | PUBLIC} [, ...]
[CASCADE | RESTRICT]
```

```sql
GRANT ALL PRIVILEGES ON kinds TO manuel;
REVOKE ALL PRIVILEGES ON kinds FROM manuel;

GRANT SELECT ON kinds TO manuel WITH GRANT OPTION; -- manuel может выдавать права на SELECT на табличку kinds
```

*Примечание:* Если мы хотим забрать grant option у manuel, то нужно использовать CASCADE, чтобы забрать права у всех, кому он выдавал права. Иначе при попытке отозвать права у manuel, запрос упадет с ошибкой.

**Примеры использования:**

```sql
CREATE USER davide WITH PASSWORD 'jw8s0F4';

GRANT CONNECT ON DATABASE db_prod TO davide;

GRANT USAGE ON SCHEMA my_schema TO davide;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA my_schema
TO davide;
```
