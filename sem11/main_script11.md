# Базы данных.

## Семинар 11. Работа с базами данных в Python. Курсоры. ORM. 

[Colab Seminar11](https://colab.research.google.com/drive/1tNwHo-HNXtEEa2NXy0eOQ5gGKCqxhOlg?usp=sharing)

### 1. Работа с базами данных в Python

[Python DB API 2.0](https://www.python.org/dev/peps/pep-0249/) - стандарт интерфейсов для пакетов, работающих с БД. "Набор правил", которым подчиняются отдельные модули, реализующие работу с конкретными базами данных.

Для PostgreSQL наиболее популярный адаптер – [psycopg2](http://initd.org/psycopg/).


### 2. Курсоры

**Курсор** – специальный объект, выполняющий запрос и получающий его результат.

Вместо того чтобы сразу выполнять весь запрос, есть возможность настроить **курсор**, инкапсулирующий запрос, и затем получать результат запроса по нескольку строк за раз. Одна из причин так делать заключается в том, чтобы избежать переполнения памяти, когда результат содержит большое количество строк.

В PL/pgSQL:
```sql
name [ [ NO ] SCROLL ] CURSOR [ ( arguments ) ] FOR query;
```

```sql
DECLARE
    curs1 refcursor;
    curs2 CURSOR FOR SELECT * FROM tenk1;
    curs3 CURSOR (key integer) FOR SELECT * FROM tenk1 WHERE unique1 = key;
```
